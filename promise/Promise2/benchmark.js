"use strict";
var Promise = require("bluebird");

var start = Date.now();
//var demoSqrt = function (num, cb) {
//    if(num < 0) {
//        return cb("number must be greater than 0");
//    }
//    var data = Math.sqrt(num);
//    cb(null, data);
//};
//var demoPrint = function (err, data) {
//    console.log(data);
//};
//
//for(let i = 0; i <= 10000; i++){
//    demoSqrt(i, demoPrint);
//}
//
var demoSqrt = function (num) {
    return new Promise(function (resolve, reject) {
        if(num < 0) {
            return reject("number must be greater than 0");
        }
        var data = Math.sqrt(num);
        resolve(data);
    });
};
var demoPrint = function (data) {
    console.log(data);

};

//for(let i = 0; i <= 10000; i++){
//    demoSqrt(i).then(demoPrint);
//}

var arryPromise = [];
for(let i = 0; i <= 10000; i++){
    arryPromise.push(demoSqrt(i).then(demoPrint));
}

Promise.all(arryPromise);

process.on("beforeExit", function () {
    console.log("Time : " + (Date.now() - start));
});
