"use strict";

var fs = require("fs");
var request = require("request");
var path = require("path");
var folderUpload = path.join(__dirname, "uploads/");
var cheerio = require("cheerio");
var Promise = require("bluebird");

var requestPromise = Promise.promisify(request);
var writeFile = Promise.promisify(fs.writeFile);


function downloader(url) {
    var filename = path.basename(url) + ".jpeg";
    if (fs.existsSync(folderUpload + filename)) {
        return console.log("File Existed");
    }
    console.log("Downloading " + url + "...");
    return requestPromise({url: url + "?fit=crop&fm=jpg&q=75&w=950", encoding: "binary"})
        .then(function (results) {
            return writeFile(folderUpload + filename, results[1], "binary");
        })
        .then(function () {
            return console.log(filename);
        })
        .catch(function (err) {
            return console.log(err);
        });
}

console.time("download");
requestPromise("https://unsplash.com/")
    .then(function (results) {
        var $ = cheerio.load(results[1]);
        var arrayImage = $("div.photo a img");
        var chainingPromise1 = Promise.resolve();
        var chainingPromise2 = Promise.resolve();
        arrayImage.map(function (key, value) {
            let fullImageLink = $(value).attr("src");
            let imagelink = fullImageLink.slice(0, fullImageLink.indexOf("?"));
            if(key % 2 === 0 ) {
                chainingPromise1 = chainingPromise1.then(function () {
                    return downloader(imagelink);
                });
            } else {
                chainingPromise2 = chainingPromise2.then(function () {
                    return downloader(imagelink);
                });
            }
        });

        Promise.all([chainingPromise1, chainingPromise2]).then(function () {
            console.timeEnd("download");
        });
    });
