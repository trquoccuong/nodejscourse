'use strict';
var fs = require('fs');
var request = require('request');
var path = require('path');
var folderUpload = __dirname + '/uploads/';
var cheerio = require('cheerio');

function downloader(url,callback) {
    var filename = path.basename(url) + '.jpeg';
    fs.exists(folderUpload + filename, function (exists) {
        if(!exists) {
            console.log("Downloading " + url + " ...")
            request({url : url + '?fit=crop&fm=jpg&q=75&w=950' ,encoding: 'binary'} , function (err,response,body) {
                if(err) {
                    callback(err)
                } else {
                   fs.writeFile(folderUpload + filename,body,'binary', function (err) {
                       if(err) {
                            callback(err)
                       } else {
                            callback(null,filename,true)
                       }
                   })
                }
            })
        } else {
            callback('File Existed');
        }
    })

}

request('https://unsplash.com/', function (req,response,body) {
    var $ = cheerio.load(body);
    var arrayImage = $('div.photo a img');
    arrayImage.map(function (key,value) {
        let fullImageLink = $(value).attr('src');
        let imagelink = fullImageLink.slice(0,fullImageLink.indexOf('?'));
        downloader(imagelink, function (err,filename) {
            if(err) {
                console.log(err);
            } else {
                console.log(filename + ' downloaded')
            }
        })
    })
})