'use strict';
var fs = require('fs');
var request = require('request');
var path = require('path');
var folderUpload = __dirname + '/uploads/';
var cheerio = require('cheerio');

function downloader(url, callback) {
    if(downloader[url]) {
        return process.nextTick(function () {
            return callback('Having same url')
        })
    }
    downloader[url] = true;
    var filename = path.basename(url) + '.jpeg';
    fs.exists(folderUpload + filename, function (exists) {
        if (!exists) {
            console.log("Downloading " + url + " ...")
            return request({
                url: url + '?fit=crop&fm=jpg&q=75&w=950',
                encoding: 'binary'
            }, function (err, response, body) {
                if (err) {
                    return callback(err)
                }
                fs.writeFile(folderUpload + filename, body, 'binary', function (err) {
                    if (err) {
                        return callback(err)
                    }
                    callback(null, filename)
                })

            })
        }
        callback('File Existed');
    })
}

downloader('https://unsplash.imgix.net/photo-1434210330765-8a00109fc773', function (err,filename) {
    console.log('a');
    if(err) {
        return console.log(err)
    }
    console.log(filename)
})

downloader('https://unsplash.imgix.net/photo-1434210330765-8a00109fc773', function (err,filename) {
    console.log('b');
    if(err) {
        return console.log(err)
    }
    console.log(filename)
})